package com.thenotoriousrog.itsbookeddisplay

/**
 * This interface is used to help update the startup activity.
 */
interface StartupActivityUpdater {
    // TODO: need to display a warning when it's time to do so.

    /**
     * This method displays a snackbar message to the users.
     * @param message - the message to display to the users in Int form. Obtained by doing R.string.some_string_reference
     * @param length - the length of the message i.e. Snackbar.LENGTH_LONG, Snackbar.LENGTH_SHORT or Snackbar.LENGTH_INDEFINITE
     */
    fun displaySnackbarMessage(message: Int, length: Int)

    fun launchWebDisplayActivity()
}