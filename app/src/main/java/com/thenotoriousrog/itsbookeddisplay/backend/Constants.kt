package com.thenotoriousrog.itsbookeddisplay.backend

/**
 * Holds various items that can be used anywhere in the application
 */
class Constants {
    companion object {
        val ITS_BOOKED_URL = "https://itsbooked.com/" // the default url to be used by the application

        val CURRENT_URL = "CurrentUrl"
        val CURRENT_ORIENTATION = "CurrentOrientation"
    }
}