package com.thenotoriousrog.itsbookeddisplay

/**
 * Helps Interact with the WebDisplayActivity to show and hide items when playing
 */
interface WebDisplayActivityUpdater {

    /**
     * This method displays a snackbar message to the users.
     * @param message - the message to display to the users in Int form. Obtained by doing R.string.some_string_reference
     * @param length - the length of the message i.e. Snackbar.LENGTH_LONG, Snackbar.LENGTH_SHORT or Snackbar.LENGTH_INDEFINITE
     */
    fun displaySnackbarMessage(message: Int, length: Int)

    /**
     * Toggles the progress bar
     * @param show - tells the method whether or not to display the progress bar.
     */
    fun toggleProgressBar(show: Boolean)
}