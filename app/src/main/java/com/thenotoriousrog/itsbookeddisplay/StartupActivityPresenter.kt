package com.thenotoriousrog.itsbookeddisplay

import android.preference.PreferenceManager
import android.support.design.widget.Snackbar
import android.util.Patterns

/**
 * Helps keep track of additional logic behaviors outside of the main logic inside the Startup
 */
class StartupActivityPresenter(private val updater: StartupActivityUpdater) {

    fun gotoAddressButtonClicked(url: String?) {
        if(url.isNullOrBlank()) {
            updater.displaySnackbarMessage(R.string.no_url_error, Snackbar.LENGTH_LONG)
        } else if (!Patterns.WEB_URL.matcher(url).matches()) {
            updater.displaySnackbarMessage(R.string.invalid_url_error, Snackbar.LENGTH_LONG)
        } else {
            updater.launchWebDisplayActivity()
        }
    }
}