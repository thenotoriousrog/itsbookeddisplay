package com.thenotoriousrog.itsbookeddisplay

/**
 * This class is used to help keep back end code away from the WebDisplayActivity to help keep the code easier to read,
 * faster, and to follow the MVP pattern.
 */
class WebDisplayActivityPresenter(private val updater: WebDisplayActivityUpdater) {
    // Not implemented yet, may not need.
}