package com.thenotoriousrog.itsbookeddisplay

import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import com.thenotoriousrog.itsbookeddisplay.backend.Constants
import kotlinx.android.synthetic.main.activity_web_display.*

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class WebDisplayActivity : AppCompatActivity(), WebDisplayActivityUpdater {

    private val TAG = "WebDisplayActivity"
    companion object {
        /**
         * Whether or not the system UI should be auto-hidden after
         * [AUTO_HIDE_DELAY_MILLIS] milliseconds.
         */
        private val AUTO_HIDE = true

        /**
         * If [AUTO_HIDE] is set, the number of milliseconds to wait after
         * user interaction before hiding the system UI.
         */
        private val AUTO_HIDE_DELAY_MILLIS = 3000

        /**
         * Some older devices needs a small delay between UI widget updates
         * and a change of the status and navigation bar.
         */
        private val UI_ANIMATION_DELAY = 300
    }

    private val presenter = WebDisplayActivityPresenter(this)
    private val handler = Handler()
    private val hideUIRunnable = Runnable {
        // Delayed removal of status and navigation bar

        // Note that some of these constants are new as of API 16 (Jelly Bean)
        // and API 19 (KitKat). It is safe to use them, as they are inlined
        // at compile-time and do nothing on earlier devices.
        fullscreen_web_content.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LOW_PROFILE or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
    }

    private val showUIRunnable = Runnable {
        // Delayed display of UI elements
        supportActionBar?.show()
    }

    private val activityMurderer = Runnable { // Only job for this runnable is to literally kill this activity. Used for fatal errors or if the user chooses to kill the activity.
        finish()
    }

    private var isUIVisible: Boolean = false
    private val hideRunnable = Runnable { hide() }

    private lateinit var webView: WebView
    private lateinit var progressBar: ProgressBar
    private lateinit var webDisplayActivityContainer: ConstraintLayout
    private lateinit var savedUrl: String
    private var savedOrientation: Int = -1

    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private val delayHideToucherListener = View.OnTouchListener { _, _ ->
        if (AUTO_HIDE) {
            delayedHide(AUTO_HIDE_DELAY_MILLIS)
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        savedUrl = intent.getStringExtra(Constants.CURRENT_URL)
        // TODO: Need to make sure we manually start the app in the proper orientation that's saved!!!
        savedOrientation = intent.getIntExtra(Constants.CURRENT_ORIENTATION, Configuration.ORIENTATION_LANDSCAPE) // Default to landscape for the saved orientation.

        Log.i(TAG, "savedUrl == $savedUrl")

        setContentView(R.layout.activity_web_display)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        isUIVisible = true

        // Set up the user interaction to manually show or hide the system UI.
        fullscreen_web_content.setOnClickListener { toggle() }

        this.progressBar = findViewById(R.id.progress_bar)
        this.webView = findViewById(R.id.fullscreen_web_content)
        this.webDisplayActivityContainer = findViewById(R.id.web_display_activity_container)

        webView.canGoBack()
        webView.canGoForward()

        webView.settings.javaScriptEnabled = true // **NOTE:
        webView.setNetworkAvailable(true)
        webView.settings.allowContentAccess = true
        webView.settings.allowFileAccess = true
        webView.settings.blockNetworkLoads = false
        webView.settings.blockNetworkImage = false
        webView.settings.builtInZoomControls = true
        webView.settings.setSupportZoom(true)
        webView.settings.domStorageEnabled = true
        webView.settings.useWideViewPort = true
        webView.settings.loadWithOverviewMode = true

        webView.webViewClient = object : WebViewClient() {

            override fun onLoadResource(view: WebView, url: String) {
                toggleProgressBar(true)
                show() // show the UI controls just to make things a little more interesting while the user waits for things to load.
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                toggleProgressBar(false)
                hide() // hide the UI controls now that the page has finished loading!
            }

            override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                Log.e(TAG, "Error recieved while loading webpage!")
                displaySnackbarMessage(R.string.fatal_web_view_error, Snackbar.LENGTH_INDEFINITE)

                // remove all callbacks and prepare for killing of this activity.
                handler.removeCallbacks(hideRunnable)
                handler.removeCallbacks(showUIRunnable)
                handler.removeCallbacks(hideUIRunnable)
                handler.postDelayed(activityMurderer, 600) // give the user a chance to read the warning and then kill the activity.
            }

        }

        webView.loadUrl(savedUrl)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100)

        toggleProgressBar(true)
    }

    private fun toggle() {
        if (isUIVisible) {
            hide()
        } else {
            show()
        }
    }

    private fun hide() {
        // Hide UI first
        supportActionBar?.hide()
        isUIVisible = false

        // Schedule a runnable to remove the status and navigation bar after a delay
        handler.removeCallbacks(showUIRunnable)
        handler.postDelayed(hideUIRunnable, UI_ANIMATION_DELAY.toLong())
    }

    private fun show() {
        // Show the system bar
        fullscreen_web_content.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        isUIVisible = true

        // Schedule a runnable to display UI elements after a delay
        handler.removeCallbacks(hideUIRunnable)
        handler.postDelayed(showUIRunnable, UI_ANIMATION_DELAY.toLong())
    }

    /**
     * Schedules a call to hide() in [delayMillis], canceling any
     * previously scheduled calls.
     */
    private fun delayedHide(delayMillis: Int) {
        handler.removeCallbacks(hideRunnable)
        handler.postDelayed(hideRunnable, delayMillis.toLong())
    }

    override fun displaySnackbarMessage(message: Int, length: Int) {
        val snackbar = Snackbar.make(webDisplayActivityContainer, message, length)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent))
        snackbar.show()
    }

    override fun toggleProgressBar(show: Boolean) {
        if(show) {
            runOnUiThread {
                this.progressBar.visibility = View.VISIBLE
            }
        } else {
            runOnUiThread {
                this.progressBar.visibility = View.GONE
            }
        }
    }

}
