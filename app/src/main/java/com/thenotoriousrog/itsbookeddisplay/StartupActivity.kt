package com.thenotoriousrog.itsbookeddisplay

import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.preference.PreferenceManager
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.design.widget.TextInputEditText
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.thenotoriousrog.itsbookeddisplay.backend.Constants

/**
 * This activity is responsible for starting up the application and helps determine which activity to display
 */
class StartupActivity : AppCompatActivity(), StartupActivityUpdater {

    private val TAG = "StartupActivity"
    private val presenter = StartupActivityPresenter(this)
    private lateinit var urlEditText: TextInputEditText
    private lateinit var goToAddressButton: ConstraintLayout
    private lateinit var startupActivityContainer: ConstraintLayout


    /**
     * Attempts to pull information from preferences in an attempt to immediately start the WebDisplayActivity
     */
    private fun tryToAutoLaunchWebDisplay() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val savedUrl = prefs.getString(Constants.CURRENT_URL, null) // default to null
        val savedOrientation = prefs.getInt(Constants.CURRENT_ORIENTATION, Configuration.ORIENTATION_LANDSCAPE) // default to landscape orientation

        if(!savedUrl.isNullOrBlank()) {
            val webShowIntent = Intent(this, WebDisplayActivity::class.java)
            webShowIntent.putExtra(Constants.CURRENT_URL, savedUrl)
            webShowIntent.putExtra(Constants.CURRENT_ORIENTATION, savedOrientation)
            startActivity(webShowIntent)
        } else {
            Log.i(TAG, "Unable to auto launch WebDisplayActivity, must wait for user to enter a URL!")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.startup_activity)

        this.startupActivityContainer = findViewById(R.id.startup_activity_container)
        this.urlEditText = findViewById(R.id.enter_url_edit_text)

        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val savedUrl = prefs.getString(Constants.CURRENT_URL, null) // default to null
        if(!savedUrl.isNullOrBlank()) {
            this.urlEditText.setText(savedUrl)
        }

        this.goToAddressButton = findViewById(R.id.go_to_address_button_layout)
        this.goToAddressButton.setOnClickListener {
            presenter.gotoAddressButtonClicked(urlEditText.text.toString())
        }

        //TODO: Need to undo the line below when ready to deliver to the client. This will keep things automated but makes testing really hard.
//        tryToAutoLaunchWebDisplay()
    }

    override fun displaySnackbarMessage(message: Int, length: Int) {
        val snackbar = Snackbar.make(startupActivityContainer, message, length)
        snackbar.view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent))
        snackbar.show()
    }

    /**
     * Saves information into shared preferences. For use later when the app is loaded back up again.
     */
    private fun storeInfo() {
        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val editor = preferences.edit()
        editor.putString(Constants.CURRENT_URL, urlEditText.text.toString())
        editor.putInt(Constants.CURRENT_ORIENTATION, resources.configuration.orientation) // **Note: This is saved for use with fireTV to prevent the user from having to change the orientation again.
        editor.apply()
    }

    override fun launchWebDisplayActivity() {
        Log.i(TAG, "launching activity now!")
        storeInfo()
        val webShowIntent = Intent(this, WebDisplayActivity::class.java)
        webShowIntent.putExtra(Constants.CURRENT_URL, urlEditText.text.toString())
        webShowIntent.putExtra(Constants.CURRENT_ORIENTATION, resources.configuration.orientation)
        startActivity(webShowIntent)
    }

}